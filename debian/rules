#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

# The custom build system doesn't support CPPFLAGS
CFLAGS:=$(CPPFLAGS) $(CFLAGS)
CXXFLAGS:=$(CPPFLAGS) $(CXXFLAGS)

# The trailing slashes are very important to the build system
subdirectories:= $(filter-out debian/, $(shell ls -d */))

# Reference for unusable modules:
# https://lists.debian.org/debian-med/2015/05/msg00075.html
unusable:= tapper/ trie/

# Note: Parallel build is not reliable
%:
	dh $@ --with python3

override_dh_auto_build:
        # The Makefile apparently doesn't use regular LDFLAGS.
        # It defines CLDFLAGS and CXXLDFLAGS as empty strings, so let's
        # use them here.
	$(MAKE) install \
	    CLDFLAGS="$(LDFLAGS)" \
	    CXXLDFLAGS="$(LDFLAGS)"
#	    WITHOUT="$(unusable)"
	mv installdir/bin/atac.pl installdir/bin/atac
	mv -v installdir/lib/halignmodule.so installdir/lib/halign.so
	mv -v installdir/lib/localAlignerInterfacemodule.so installdir/lib/localAlignerInterface.so

override_dh_auto_clean:
	$(MAKE) real-clean
	rm -rf installdir

override_dh_install:
	dh_install
	for perlscript in `grep -l '#!/usr/bin/env \+perl' debian/*/usr/bin/*` `grep -l '#!/usr/bin/env \+perl' debian/*/usr/lib/atac/bin/*` ; do \
	    sed -i '1s+#!/usr/bin/env perl+#!/usr/bin/perl+' $${perlscript} ; \
	done

override_dh_python3:
	dh_python3 --no-ext-rename

get-orig-source:
	DEB_VERSION_UPSTREAM=$(DEB_VERSION_UPSTREAM) debian/get-orig-source
